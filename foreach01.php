<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>my title</title>
</head>
<body>

<pre>
<?php
    //$ar1 = array(2,4,6,8,10, 'abc', 'john');
    //$ar1[100] = 'hello';

    /*
    $ar1 = array(
        'name' => 'david',
        'age' => 25,
        'gender' => 'male',
        'hi',
    );
    */
    $ar1 = [
        'name' => 'david',
        'age' => 25,
        'gender' => 'male',
        'hi',
    ];

    foreach($ar1 as $a => $b){
        echo " $a => $b <br>";
    }



?>
</pre>


</body>
</html>
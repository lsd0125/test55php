-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- 主機： 127.0.0.1
-- 產生時間： 2019 年 07 月 05 日 05:04
-- 伺服器版本： 10.3.16-MariaDB
-- PHP 版本： 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `test_db`
--

-- --------------------------------------------------------

--
-- 資料表結構 `address_book`
--

CREATE TABLE `address_book` (
  `sid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `birthday` date NOT NULL DEFAULT '1999-06-06'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 傾印資料表的資料 `address_book`
--

INSERT INTO `address_book` (`sid`, `name`, `email`, `mobile`, `address`, `birthday`) VALUES
(1, '李小明', 'small-ming@gg.com', '0918123456', '台北市', '1999-06-18'),
(2, '李小明', 'small-ming@gg.com', '0918123456', '台北市', '1999-06-18'),
(3, '李小明', 'small-ming@gg.com', '0918123456', '台北市', '1999-06-18'),
(4, '李大明', 'small-ming@gg.com', '0918123456', '台北市', '1999-06-18'),
(5, '李小明2', 'small-ming@gg.com', '0918123456', '台北市', '1999-06-18'),
(6, '李小明3', 'small-ming@gg.com', '0918123456', '台北市', '1999-06-18'),
(7, '李小明4', 'small-ming@gg.com', '0918123456', '台北市', '1999-06-18'),
(8, '李小明5', 'small-ming@gg.com', '0918123456', '台北市', '1999-06-18'),
(9, '李小明6', 'small-ming@gg.com', '0918123456', '台北市', '1999-06-18'),
(10, '李小明7', 'small-ming@gg.com', '0918123456', '台北市', '1999-06-18'),
(11, '李小明8', 'small-ming@gg.com', '0918123456', '台北市', '1999-06-18'),
(12, '李小明9', 'small-ming@gg.com', '0918123456', '台北市', '1999-06-18');

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `address_book`
--
ALTER TABLE `address_book`
  ADD PRIMARY KEY (`sid`);

--
-- 在傾印的資料表使用自動遞增(AUTO_INCREMENT)
--

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `address_book`
--
ALTER TABLE `address_book`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

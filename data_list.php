<?php
require __DIR__. '/__db_connect.php';

$page_name = 'data_list';

// 用戶決定觀看的頁數
$page = isset($_GET['page']) ? intval($_GET['page']) : 1;

$per_page = 5; // 每頁有幾筆

$t_sql = "SELECT COUNT(1) FROM `address_book` ";
// 取得總筆數
$total_rows = $pdo->query($t_sql)->fetch(PDO::FETCH_NUM)[0];

$total_pages = ceil($total_rows/$per_page);

// 頁碼限定範圍
if($page<1){
    // $page = 1;
    header('Location: data_list.php');
    exit;
}

if($page>$total_pages){
    // $page = $total_pages;
    header('Location: data_list.php');
    exit;
}



$sql = sprintf("SELECT * FROM `address_book`
                ORDER BY `sid` DESC LIMIT %s, %s", ($page-1)*$per_page, $per_page);
$stmt = $pdo->query($sql);



?>
<?php include __DIR__. '/__html_head.php' ?>

<?php include '__navbar.php' ?>
<div class="container">

    <div class="row" style="margin-top: 2rem;">
        <div class="col-lg-12">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item <?= 1==$page ? 'disabled' : '' ?>">
                        <a class="page-link" href="?page=<?= $page-1 ?>">
                            <i class="fas fa-arrow-circle-left"></i>
                        </a>
                    </li>
                    <?php for($i=1; $i<=$total_pages; $i++): ?>
                    <li class="page-item <?= $i==$page ? 'active' : '' ?>">
                        <a class="page-link" href="?page=<?= $i ?>"><?= $i ?></a>
                    </li>
                    <?php endfor ?>
                    <li class="page-item <?= $total_pages==$page ? 'disabled' : '' ?>">
                        <a class="page-link" href="?page=<?= $page+1 ?>">
                            <i class="fas fa-arrow-circle-right"></i>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>



    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col"><i class="fas fa-trash-alt"></i></th>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Mobile</th>
            <th scope="col">Address</th>
            <th scope="col">Birthday</th>
            <th scope="col">
                <i class="fas fa-edit"></i>
            </th>
        </tr>
        </thead>
        <tbody>
        <?php while($row=$stmt->fetch()): ?>
        <tr>
            <td>
                <a href="javascript: delete_one(<?= $row['sid'] ?>)" class="btn btn-danger">
                    <i class="fas fa-trash-alt"></i>
                </a>
            </td>
            <td><?= $row['sid'] ?></td>
            <td><?= htmlentities($row['name']) ?></td>
            <td><?= htmlentities($row['email']) ?></td>
            <td><?= htmlentities($row['mobile']) ?></td>
            <td><?= strip_tags($row['address']) ?></td>
            <td><?= htmlentities($row['birthday']) ?></td>
            <td>
                <a href="data_edit.php?sid=<?= $row['sid'] ?>" class="btn btn-primary">
                    <i class="fas fa-edit"></i>
                </a>
            </td>
        </tr>
        <?php endwhile ?>
        </tbody>
    </table>

</div>
<script>
    function delete_one(sid){
        if(confirm(`確定要刪除編號為 ${sid} 的資料嗎?`)){
            location.href = 'data_delete.php?sid=' + sid;
        }
    }
</script>
<?php include __DIR__. '/__html_foot.php' ?>

<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= __FILE__ ?></title>
</head>
<body>

<table border="1">
<?php
for ($i = 1; $i < 10; $i++) {
    echo '<tr>';
    for ($k = 1; $k < 10; $k++) {
        echo "<td> $i * $k = ". $i*$k. "</td>";
    }
    echo '</tr>';
}
?>
</table>


<?php
    // isset() 用來判斷變數有沒有設定過
    echo isset($k) ? 'true<br>' : 'false<br>';
    echo isset($b) ? 'true<br>' : 'false<br>';
    unset($k);
    echo isset($k) ? 'true<br>' : 'false<br>';



?>



</body>
</html>
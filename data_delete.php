<?php
require __DIR__. '/__db_connect.php';

$sid = isset($_GET['sid']) ? intval($_GET['sid']) : 0;

if(empty($sid)) exit; // 如果 sid 是 0, 就結束程式

$sql = "DELETE FROM `address_book` WHERE `sid`=$sid";

$pdo->query($sql);

if(empty($_SERVER['HTTP_REFERER'])){
    header('Location: data_list.php');
} else {
    header('Location: '. $_SERVER['HTTP_REFERER']);
}








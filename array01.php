<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>my title</title>
</head>
<body>

<pre>
<?php
    $ar1 = array(2,4,6,8,10);

    $ar2 = [2,4,6,8,10];

    $ar3[] = 6; // array push
    $ar3[] = 7;
    $ar3[] = 8;
    $ar3[] = 9;
    $ar3[] = 10;
    $ar3[] = array(1,2,3);
    $ar3[10] = 100;

    //echo $ar3;

    print_r($ar3);

    var_dump($ar3);

?>
</pre>


</body>
</html>
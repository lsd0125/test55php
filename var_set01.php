<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>my title</title>
</head>
<body>

<pre>
<?php
    $ar = [
        'num'=>17,
        'name'=>'david',
    ];

    $br = $ar; // copy
    $cr = &$ar; // 設定別名

    $ar['name'] = 'john';

    print_r($ar);
    print_r($br);
    print_r($cr);

?>
</pre>


</body>
</html>
<?php
require __DIR__. '/__db_connect.php';

$result = [
    'success' => false,
    'code' => 0,
    'info' => '',
    'post' => $_POST,

];

// TODO: 檢查欄位

$stmt = $pdo->prepare("INSERT INTO `address_book`(
        `name`, `email`, `mobile`, `address`, `birthday`
        ) VALUES (?, ?, ?, ?, ?)");

$stmt->execute([
        $_POST['name'],
        $_POST['email'],
        $_POST['mobile'],
        $_POST['address'],
        $_POST['birthday'],
]);

if($stmt->rowCount()==1){
    $result['success'] = true;
    $result['info'] = '資料新增成功';
} else {
    $result['info'] = '資料新增錯誤';
    $result['code'] = 400;
}

echo json_encode($result, JSON_UNESCAPED_UNICODE);



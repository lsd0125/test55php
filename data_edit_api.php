<?php
require __DIR__. '/__db_connect.php';

$result = [
    'success' => false,
    'code' => 0,
    'info' => '找不到該筆資料',
    'post' => $_POST,

];

// TODO: 檢查欄位

// 如果沒有 主鍵資料 就不修改
if(empty($_POST['sid'])){
    echo json_encode($result, JSON_UNESCAPED_UNICODE);
    exit;
}


$stmt = $pdo->prepare("UPDATE `address_book` SET 
                    `name`=?, 
                    `email`=?, 
                    `mobile`=?, 
                    `address`=?, 
                    `birthday`=? 
                    WHERE `sid`=?");

$stmt->execute([
        $_POST['name'],
        $_POST['email'],
        $_POST['mobile'],
        $_POST['address'],
        $_POST['birthday'],
        $_POST['sid'],
]);

if($stmt->rowCount()==1){
    $result['success'] = true;
    $result['info'] = '資料修改成功';
} else {
    $result['info'] = '資料沒有修改';
    $result['code'] = 400;
}

echo json_encode($result, JSON_UNESCAPED_UNICODE);



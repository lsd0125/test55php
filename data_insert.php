<?php
require __DIR__. '/__db_connect.php';

$page_name = 'data_insert';

?>
<?php include __DIR__. '/__html_head.php' ?>
<style>
    small{
        color: red;
    }
</style>
<?php include '__navbar.php' ?>
<div class="container">

    <div class="row" style="margin-top: 2rem;">

        <div class="col-lg-6">
            <div id="alertInfo" class="alert alert-primary" role="alert" style="display: none;">
            </div>
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">新增資料</h5>
                    <form id="myform" method="post" onsubmit="return checkForm()">
                        <div class="form-group">
                            <label for="name">姓名</label>
                            <input type="text" class="form-control" id="name" name="name" value="姓名test">
                            <small class="form-text"></small>
                        </div>
                        <div class="form-group">
                            <label for="email">電郵</label>
                            <input type="text" class="form-control" id="email" name="email" value="email@ggg.com">
                            <small class="form-text"></small>
                        </div>
                        <div class="form-group">
                            <label for="mobile">手機</label>
                            <input type="text" class="form-control" id="mobile" name="mobile" value="0935-123-456">
                            <small class="form-text"></small>
                        </div>
                        <div class="form-group">
                            <label for="birthday">生日</label>
                            <input type="text" class="form-control" id="birthday" name="birthday" placeholder="生日">
                        </div>
                        <div class="form-group">
                            <label for="address">地址</label>
                            <textarea class="form-control" id="address" name="address" rows="5"></textarea>
                        </div>
                        <button id="submitBtn" type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var alertInfo = $('#alertInfo');
    var submitBtn = $('#submitBtn');
    var $name = $('#name');
    var $email = $('#email');
    var $mobile = $('#mobile');
    var fields = [$name, $email, $mobile];

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    function checkForm() {
        // 先回復到原來的狀態
        fields.forEach(function(val){
            val.next().text('');
        });
        alertInfo.hide();
        submitBtn.hide();

        var isPass = true; // 表單是否有通過檢查
        var mobileRegex = /^09\d{2}\-?\d{3}\-?\d{3}$/;

        if($name.val().length < 2) {
            isPass = false;
            $name.next().text('請輸入兩個以上的字元');
        }

        if(! validateEmail($email.val())) {
            isPass = false;
            $email.next().text('請輸入正確的 Email 格式');
        }
        if(! mobileRegex.test($mobile.val())){
            isPass = false;
            $mobile.next().text('請輸入正確的手機格式');
        }

        if(isPass){

            $.post('data_insert_api.php', $('#myform').serialize(), function(data){
                console.log(data);

                // alert(data.info);

                if(data.success){
                    //location.href = 'data_list.php';
                    alertInfo.removeClass('alert-danger');
                    alertInfo.addClass('alert-success');
                } else {
                    alertInfo.removeClass('alert-success');
                    alertInfo.addClass('alert-danger');
                }
                alertInfo.text(data.info);
                alertInfo.show();
                submitBtn.show();
            }, 'json');
        }
        return false;
    }
</script>
<?php include __DIR__. '/__html_foot.php' ?>

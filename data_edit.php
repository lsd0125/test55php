<?php
require __DIR__. '/__db_connect.php';

$page_name = 'data_edit';

$sid = isset($_GET['sid']) ? intval($_GET['sid']) : 0;
if(empty($sid)) {
    header('Location: data_list.php'); exit;
}

// TODO: 讀取指定的資料
$sql = "SELECT * FROM `address_book` WHERE `sid`=". $sid;
$stmt = $pdo->query($sql);

$row = $stmt->fetch(PDO::FETCH_ASSOC);
if(empty($row)){
    header('Location: data_list.php'); exit;
}

?>
<?php include __DIR__. '/__html_head.php' ?>
<style>
    small{
        color: red;
    }
</style>
<?php include '__navbar.php' ?>
<div class="container">

    <div class="row" style="margin-top: 2rem;">

        <div class="col-lg-6">
            <div id="alertInfo" class="alert alert-primary" role="alert" style="display: none;">
            </div>
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">編輯資料 #<?= $row['sid'] ?></h5>
                    <form id="myform" method="post" onsubmit="return checkForm()">
                        <input type="hidden" name="sid" value="<?= $row['sid'] ?>">
                        <div class="form-group">
                            <label for="name">姓名</label>
                            <input type="text" class="form-control" id="name" name="name" value="<?= htmlentities($row['name']) ?>">
                            <small class="form-text"></small>
                        </div>
                        <div class="form-group">
                            <label for="email">電郵</label>
                            <input type="text" class="form-control" id="email" name="email" value="<?= htmlentities($row['email']) ?>">
                            <small class="form-text"></small>
                        </div>
                        <div class="form-group">
                            <label for="mobile">手機</label>
                            <input type="text" class="form-control" id="mobile" name="mobile" value="<?= htmlentities($row['mobile']) ?>">
                            <small class="form-text"></small>
                        </div>
                        <div class="form-group">
                            <label for="birthday">生日</label>
                            <input type="text" class="form-control" id="birthday" name="birthday" value="<?= htmlentities($row['birthday']) ?>">
                        </div>
                        <div class="form-group">
                            <label for="address">地址</label>
                            <textarea class="form-control" id="address"
                                      name="address" rows="5"><?= htmlentities($row['address']) ?></textarea>
                        </div>
                        <button id="submitBtn" type="submit" class="btn btn-primary">修改</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var alertInfo = $('#alertInfo');
    var submitBtn = $('#submitBtn');
    var $name = $('#name');
    var $email = $('#email');
    var $mobile = $('#mobile');
    var fields = [$name, $email, $mobile];

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    function checkForm() {
        // 先回復到原來的狀態
        fields.forEach(function(val){
            val.next().text('');
        });
        alertInfo.hide();
        submitBtn.hide();

        var isPass = true; // 表單是否有通過檢查
        var mobileRegex = /^09\d{2}\-?\d{3}\-?\d{3}$/;

        if($name.val().length < 2) {
            isPass = false;
            $name.next().text('請輸入兩個以上的字元');
        }

        if(! validateEmail($email.val())) {
            isPass = false;
            $email.next().text('請輸入正確的 Email 格式');
        }
        if(! mobileRegex.test($mobile.val())){
            isPass = false;
            $mobile.next().text('請輸入正確的手機格式');
        }

        if(isPass){

            $.post('data_edit_api.php', $('#myform').serialize(), function(data){
                console.log(data);

                // alert(data.info);

                if(data.success){
                    //location.href = 'data_list.php';
                    alertInfo.removeClass('alert-danger');
                    alertInfo.addClass('alert-success');
                } else {
                    alertInfo.removeClass('alert-success');
                    alertInfo.addClass('alert-danger');
                }
                alertInfo.text(data.info);
                alertInfo.show();
                submitBtn.show();
            }, 'json');
        }
        return false;
    }
</script>
<?php include __DIR__. '/__html_foot.php' ?>
